#!/usr/bin/python
# Scrape information from the WSU Handbook
# Author: Laurence Park, 2019


import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
#import re


def download_page(unit_number):
    # tell handbook that this is Chrome
    ua = UserAgent()
    header = {'User-Agent':str(ua.chrome)}
    
    url_base = "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit="
    # create URL
    url = url_base + unit_number
    # download page
    response = requests.get(url, headers=header)
    # parse page
    soup = BeautifulSoup(response.content, "html.parser")
    return(soup)

    
# cleaning functions
def check_empty_sibling(x):
    if (not x):
        return(None)
    else:
        return(x.next_sibling.next_sibling.replace('\n','').replace('\t','').replace('\r',' '))

def seperate_list(x, sep):
    if (not x):
        return(None)
    x_set = x.split(sep)
    x_set = [y.strip() for y in x_set]
    return(x_set)


def clean_unit_set(x):
    y = seperate_list(x, ';')
    if (y and (len(y) == 1)):
        return(seperate_list(y[0], ','))
    return(y)


def clean_prereqs(x):
    if (not x):
        return(None)
    y_set = seperate_list(x, 'AND')
    z = [seperate_list(y, 'OR') for y in y_set]
    return(z)


def extract_unit_information(soup):
    # extract information
    unit_information = {
        "school" : soup.find_all('span')[0].text,
        "unit_name" : soup.find_all('span')[1].text,
        "unit_number" : soup.find_all('span')[2].next_sibling.split('.')[0],
        "unit_version" : soup.find_all('span')[2].next_sibling.split('.')[1],
        "discipline" : soup.find_all('span')[3].next_sibling,
        "student_contribution_band" : soup.find_all('span')[4].next_sibling,
        "unit_level" : soup.find_all('span')[5].next_sibling,
        "cps" : soup.find_all('span')[6].next_sibling,
        "coordinator" : soup.find_all('a')[13].text,
        "prerequisites" : clean_prereqs(check_empty_sibling(soup.find('span', string = "Prerequisite"))),
        "enrolment_restrictions" : check_empty_sibling(soup.find('span', string = "Unit Enrolment Restrictions")),
        "equivalent_units" : clean_unit_set(check_empty_sibling(soup.find('span', string = "Equivalent Units"))),
        "incompatible_units" : clean_unit_set(check_empty_sibling(soup.find('span', string = "Incompatible Units"))),
        "assumed_knowledge" : check_empty_sibling(soup.find('span', string = "Assumed Knowledge"))
    }
    return(unit_information)


#unit_number = "300597"
#unit_number = "301114"
unit_number = "301119"
#unit_number = "300579"
#unit_number = "102109"
#unit_number = "301196"

# download parsed HTML
soup = download_page(unit_number)
# extract information
unit_information = extract_unit_information(soup)

print(unit_information)
